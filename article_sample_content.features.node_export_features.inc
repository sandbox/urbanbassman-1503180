<?php
/**
 * @file
 * article_sample_content.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function article_sample_content_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  array(
    \'vid\' => \'6\',
    \'uid\' => \'1\',
    \'title\' => \'Drive a real train!\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'c87bdcaf-2962-9194-c1d6-8f64d0b4947c\',
    \'nid\' => \'6\',
    \'type\' => \'article\',
    \'language\' => \'und\',
    \'created\' => \'1332787959\',
    \'changed\' => \'1332787959\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'36b9dc95-0c4c-0c94-d1de-1a2bc6b63a73\',
    \'revision_timestamp\' => \'1332787959\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>The ultimate gift or the chance to live a childhood ambition.</p><p>Drive a steam or diesel locomotive on the South Devon Railway under the watchful eye of our experienced footplate crews.</p><p>The South Devon Railway can make it possible &ndash; with a Steam or Diesel Footplate Experience day. Steam experience days cost &pound;385 per person. Diesel experience days cost &pound;250 per person. Up to six guests, who will ride in the train, are free. All courses are subject to availability.</p><p>You must be over the age of 18 years and be fit and healthy. You will need to wear old clothes and stout shoes. We will supply a dustcoat and gloves.</p><p>South Devon Railway footplate experience courses make a superb and memorable gift. The current list of dates is on the booking form (link below), but we recommend you check dates and availability with us by calling 0843 357 1420 or emailing us. Either book by phone, by email or fill in the booking form and send it to us.</p><p>What our customers say!</p><p>&quot;When I found out about my footplate experience on my birthday this year (it was a present from my wife, Valerie) I was delighted about the present but shocked at the price she had to pay for it. This is because I thought we would be just riding on the footplate of a service train. After the actual experience I now realise that it&#39;s fantastic value for money and I really don&#39;t know how you do it for the price.</p><p>You see, at no time was it clear to me.....that you were going to provide a private train for &#39;experiencees&#39; and their guests. You do make reference to the fact that we would be passing the service train so it is implicit in your material but I really think you should make it explicit and trumpet the words - YOUR OWN PRIVATE TRAIN - from the rooftops!</p><p>Thank you again for a wonderful day.</p><p>Terry McAuliffe&quot;</p>\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>The ultimate gift or the chance to live a childhood ambition.</p>
<p>Drive a steam or diesel locomotive on the South Devon Railway under the watchful eye of our experienced footplate crews.</p>
<p>The South Devon Railway can make it possible &ndash; with a Steam or Diesel Footplate Experience day. Steam experience days cost &pound;385 per person. Diesel experience days cost &pound;250 per person. Up to six guests, who will ride in the train, are free. All courses are subject to availability.</p>
<p>You must be over the age of 18 years and be fit and healthy. You will need to wear old clothes and stout shoes. We will supply a dustcoat and gloves.</p>
<p>South Devon Railway footplate experience courses make a superb and memorable gift. The current list of dates is on the booking form (link below), but we recommend you check dates and availability with us by calling 0843 357 1420 or emailing us. Either book by phone, by email or fill in the booking form and send it to us.</p>
<p>What our customers say!</p>
<p>&quot;When I found out about my footplate experience on my birthday this year (it was a present from my wife, Valerie) I was delighted about the present but shocked at the price she had to pay for it. This is because I thought we would be just riding on the footplate of a service train. After the actual experience I now realise that it&#39;s fantastic value for money and I really don&#39;t know how you do it for the price.</p>
<p>You see, at no time was it clear to me.....that you were going to provide a private train for &#39;experiencees&#39; and their guests. You do make reference to the fact that we would be passing the service train so it is implicit in your material but I really think you should make it explicit and trumpet the words - YOUR OWN PRIVATE TRAIN - from the rooftops!</p>
<p>Thank you again for a wonderful day.</p>
<p>Terry McAuliffe&quot;</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_tags\' => array(),
    \'field_content_image\' => array(),
    \'field_media\' => array(),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332787959\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clyde\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'7\',
      \'source\' => \'node/6\',
      \'alias\' => \'article/drive-real-train\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
    array(
    \'vid\' => \'4\',
    \'uid\' => \'1\',
    \'title\' => \'Heritage Railway Welcomes the Olympic Torch\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'4cb29cf7-139e-ae34-49e5-13712a550e9d\',
    \'nid\' => \'4\',
    \'type\' => \'article\',
    \'language\' => \'und\',
    \'created\' => \'1332784636\',
    \'changed\' => \'1332784636\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'90a39376-477a-ece4-0945-4cdbd550efc2\',
    \'revision_timestamp\' => \'1332784636\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>It is with great pleasure that we are able to announce that the Olympic Torch will, as part of its tour of the country, be carried on the Heritage Railway on Thursday 24th May.</p>

<p>The torch will arrive at Bewdley station at approximately 4.00pm and will be carried by a special train, appropriately named \\\'The Worcestershire Express\\\', to Kidderminster where it will continue its journey by road.</p>

<p>The Railway will continue to operate normally on May 24th with visitors most welcome to be with us for this once in a lifetime occasion.</p>

<p>On this special day we will be operating an enhanced train service. Timetable \\\'B\\\' will operate throughout the day instead of Timetable \\\'A\\\' as previously advertised.</p>

<p>Full details of train times can be found at our Calendar & Timetable page.</p>

<p>Our best wishes go to all the athletes taking part in LONDON 2012!</p>

<p>We look forward to seeing you on May 24th.</p>
\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>It is with great pleasure that we are able to announce that the Olympic Torch will, as part of its tour of the country, be carried on the Heritage Railway on Thursday 24th May.</p>
<p>The torch will arrive at Bewdley station at approximately 4.00pm and will be carried by a special train, appropriately named \\\'The Worcestershire Express\\\', to Kidderminster where it will continue its journey by road.</p>
<p>The Railway will continue to operate normally on May 24th with visitors most welcome to be with us for this once in a lifetime occasion.</p>
<p>On this special day we will be operating an enhanced train service. Timetable \\\'B\\\' will operate throughout the day instead of Timetable \\\'A\\\' as previously advertised.</p>
<p>Full details of train times can be found at our Calendar &amp; Timetable page.</p>
<p>Our best wishes go to all the athletes taking part in LONDON 2012!</p>
<p>We look forward to seeing you on May 24th.</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_tags\' => array(),
    \'field_content_image\' => array(),
    \'field_media\' => array(),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332784636\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clyde\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'5\',
      \'source\' => \'node/4\',
      \'alias\' => \'article/heritage-railway-welcomes-olympic-torch\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
    array(
    \'vid\' => \'5\',
    \'uid\' => \'1\',
    \'title\' => \'Our Photographers Acknowledged\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'325161ba-115a-33b4-e953-170be8ca1648\',
    \'nid\' => \'5\',
    \'type\' => \'article\',
    \'language\' => \'und\',
    \'created\' => \'1332784768\',
    \'changed\' => \'1332784768\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'c74cfcef-2474-95d4-8933-0667934483af\',
    \'revision_timestamp\' => \'1332784768\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>The Heritage Railway is grateful for the use of photographs and other pictorial material freely supplied by the following enthusiasts and photographers.</p>

<p>These pictures may appear here on our website or in various SVR publications.</p>

<p>We are indebted to:</p>

<p>John Austin - For the reproduction of his original oil paintings</p>

<p>and to:</p>

<p>Jed Bennett, Paul Chancellor, Jason Houlders, Ray Jones, Scott Lewis, David Pagett, John Oates, Malcolm Ranieri & Bob Sweet - for the reproduction of photographs.</p>

<p>We are also grateful to the many photographers who provide material from time to time and for which we have no record of ownership.</p>

<p>If you regularly take photographs of our trains and stations and would like your work considered for publication please contact our Visitor Services Department on the telephone number given below.</p>

<p>Please note: If you have supplied pictorial material that is not acknowledged here, please let us know on 01299 403816.</p>\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>The Heritage Railway is grateful for the use of photographs and other pictorial material freely supplied by the following enthusiasts and photographers.</p>
<p>These pictures may appear here on our website or in various SVR publications.</p>
<p>We are indebted to:</p>
<p>John Austin - For the reproduction of his original oil paintings</p>
<p>and to:</p>
<p>Jed Bennett, Paul Chancellor, Jason Houlders, Ray Jones, Scott Lewis, David Pagett, John Oates, Malcolm Ranieri &amp; Bob Sweet - for the reproduction of photographs.</p>
<p>We are also grateful to the many photographers who provide material from time to time and for which we have no record of ownership.</p>
<p>If you regularly take photographs of our trains and stations and would like your work considered for publication please contact our Visitor Services Department on the telephone number given below.</p>
<p>Please note: If you have supplied pictorial material that is not acknowledged here, please let us know on 01299 403816.</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_tags\' => array(),
    \'field_content_image\' => array(),
    \'field_media\' => array(),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332784768\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clyde\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'6\',
      \'source\' => \'node/5\',
      \'alias\' => \'article/our-photographers-acknowledged\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
)',
);
  return $node_export;
}
